import pytest
from pydantic import ValidationError
from typing import Any

from cim_client.models import (
    PersonBase,
    OrgImportIdMixin,
    NextOfKinMixin,
    Organisation,
)


class Person(PersonBase, OrgImportIdMixin, NextOfKinMixin):
    pass


def test_person(john_doe: Any, jane_doe: Any, fai_lure: Any) -> None:
    persons = (john_doe, jane_doe)
    # Check correct examples work
    for data in persons:
        Person.from_dict(data)

    # Check missing username and import id fails
    with pytest.raises(ValidationError):
        Person.from_dict(fai_lure)


def test_organisation() -> None:
    a_dict = {"name": "name", "key": "key"}
    Organisation.from_dict(a_dict)
    b_dict = {"name": "name", "key": "key", "parent_key": "parent_key"}
    Organisation.from_dict(b_dict)


def test_organisation_fails_with_extra_field() -> None:
    with pytest.raises(ValidationError):
        Organisation.from_dict(
            {
                "name": "name",
                "key": "key",
                "parent_key": "parent_key",
                "extra field": "x",
            }
        )

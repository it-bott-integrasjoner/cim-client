import logging
import pytest
from requests import HTTPError
from typing import Any, Dict, List, Type

from cim_client.client import (
    CimClient,
    CimEndpoints,
    DeletePersonReply,
    Organisation,
    logger as client_logger,
    _find_unknown_parents,
    _serialise_upsert_person,
)
from cim_client.models import (
    PersonBase,
    OrgImportIdMixin,
    NextOfKinMixin,
    OrganisationImportResult,
)


class Person(PersonBase, OrgImportIdMixin, NextOfKinMixin):
    pass


@pytest.fixture
def header_name() -> str:
    return "X-Test"


@pytest.fixture
def client_cls(header_name: str) -> Type[CimClient]:
    class TestClient(CimClient):
        default_headers = {
            header_name: "6a9a32f0-7322-4ef3-bbce-6685a3388e67",
        }

    return TestClient


def test_init_does_not_mutate_arg(client_cls: Type[CimClient], baseurl: str) -> None:
    headers: Dict[str, str] = {}
    client = client_cls(CimEndpoints(baseurl), headers=headers)
    assert headers is not client.headers
    assert not headers


def test_init_applies_default_headers(
    client_cls: Type[CimClient], baseurl: str, header_name: str
) -> None:
    headers: Dict[str, str] = {}
    client = client_cls(CimEndpoints(baseurl), headers=headers)
    assert header_name in client.headers
    assert client.headers[header_name] == client.default_headers[header_name]


def test_init_modify_defaults(
    client_cls: Type[CimClient], baseurl: str, header_name: str
) -> None:
    headers = {header_name: "ede37fdd-a2ae-4a96-9d80-110528425ea6"}
    client = client_cls(CimEndpoints(baseurl), headers=headers)
    # Check that we respect the headers arg, and don't use default_headers
    assert client.headers[header_name] == headers[header_name]
    # Check that we don't do this by mutating default_headers
    assert client.default_headers[header_name] != headers[header_name]


def test_init_from_dict(config_example: Any) -> None:
    client = CimClient(**config_example)

    for k, v in config_example["headers"].items():
        assert client.headers[k] == v

    assert client.return_objects == config_example["return_objects"]
    assert client.auth == tuple(config_example["auth"])

    e = client.endpoints
    assert isinstance(e, CimEndpoints)
    assert e.baseurl == config_example["endpoints"]["url"]
    assert e.upsert_persons_url == "upsert_persons_url"
    assert e.delete_persons_url == "delete_persons_url"
    assert e.import_organisations_url == "import_organisations_url"
    assert e.baseurl == "https://example.com"


def test_get_upsert_persons_schema(client: CimClient, requests_mock: Any) -> None:
    """Ensure getting upsert schema works"""
    requests_mock.get(
        "https://localhost/_webservices/?ws=contacts/upsert/1.0", json={"foo": "bar"}
    )
    response = client.get_upsert_persons_schema()
    assert response == {"foo": "bar"}


def test_get_delete_persons_schema(client: CimClient, requests_mock: Any) -> None:
    """Ensure getting delete schema works"""
    requests_mock.get(client.endpoints.delete_persons(), json={"foo": "bar"})
    response = client.get_delete_persons_schema()
    assert response == {"foo": "bar"}


def test_delete_persons_success_200(
    client: CimClient, john_doe: Any, requests_mock: Any
) -> None:
    """Ensure 200 returns proper response"""
    requests_mock.post(
        client.endpoints.delete_persons(),
        status_code=200,
        json=["Delete person: johndoe(9999)"],
    )
    reply, response = client.delete_persons([Person.from_dict(john_doe).username])

    assert reply == DeletePersonReply.success
    assert response.status_code == 200
    assert response.content == b'["Delete person: johndoe(9999)"]'


def test_delete_persons_success_404(
    client: CimClient, john_doe: Any, requests_mock: Any, caplog: Any
) -> None:
    """Ensure 404 returns None when response text is `No person found: No deletion`"""
    caplog.set_level(level=logging.DEBUG, logger=client_logger.name)
    requests_mock.post(
        client.endpoints.delete_persons(),
        status_code=404,
        text="No person found: No deletion",
    )
    reply, response = client.delete_persons([Person.from_dict(john_doe).username])

    assert reply == DeletePersonReply.no_deletion
    assert response.status_code == 404
    assert response.content == b"No person found: No deletion"
    assert next(
        x
        for x in caplog.records
        if x.message
        == "No persons were deleted, response was: b'No person found: No deletion'"
        and x.levelno == logging.DEBUG
    )


def test_delete_persons_failure_404(
    client: CimClient, john_doe: Any, requests_mock: Any
) -> None:
    """Ensure 404 fails when response text is not `No person found: No deletion`"""
    requests_mock.post(
        client.endpoints.delete_persons(),
        status_code=404,
        text='["Something, something, something dark side"]',
    )
    with pytest.raises(HTTPError):
        client.delete_persons([Person.from_dict(john_doe).username])


def test_upsert_persons_one(
    client: CimClient, john_doe: Any, requests_mock: Any
) -> None:
    """Ensure upserting one works"""
    requests_mock.post(client.endpoints.upsert_persons())
    response = client.upsert_persons(Person.from_dict(john_doe))

    assert response.status_code == 200


def test_upsert_persons_many(
    client: CimClient, john_doe: Any, jane_doe: Any, requests_mock: Any
) -> None:
    """Ensure upserting multiple works"""
    personlist = [Person.from_dict(i) for i in (john_doe, jane_doe)]

    requests_mock.post(client.endpoints.upsert_persons())
    response = client.upsert_persons(personlist)  # type: ignore[arg-type]

    assert response.status_code == 200


def test_upsert_persons_one_wrong_type_raises_type_error(
    client: CimClient, john_doe: Any, requests_mock: Any
) -> None:
    with pytest.raises(TypeError):
        client.upsert_persons("Sesam, sesam")  # type: ignore[arg-type]


def test_upsert_persons_many_wrong_type_raises_type_error(
    client: CimClient, john_doe: Any, requests_mock: Any
) -> None:
    data = [Person.from_dict(john_doe), 12]

    with pytest.raises(TypeError):
        client.upsert_persons(data)  # type: ignore


def test_upsert_persons_failure_400(
    client: CimClient, john_doe: Any, requests_mock: Any
) -> None:
    """Ensure 400 raises an error"""
    requests_mock.post(client.endpoints.upsert_persons(), status_code=400)
    with pytest.raises(HTTPError):
        client.upsert_persons(Person.from_dict(john_doe))


def test_upsert_persons_failure_500(
    client: CimClient, john_doe: Any, requests_mock: Any
) -> None:
    """Ensure 500 raises an error"""
    requests_mock.post(client.endpoints.upsert_persons(), status_code=500)
    with pytest.raises(HTTPError):
        client.upsert_persons(Person.from_dict(john_doe))


def test_get_import_organisations_schema(client: CimClient, requests_mock: Any) -> None:
    """Ensure getting import organisations schema works"""
    requests_mock.get(
        client.endpoints.import_organisations(),
        json={"foo": "bar"},
    )
    response = client.get_import_organisations_schema()
    assert response == {"foo": "bar"}


def test_import_organisations(
    client: CimClient,
    organisations: List[Organisation],
    organisations_json: Any,
    requests_mock: Any,
) -> None:
    adapter = requests_mock.post(
        client.endpoints.import_organisations(),
        json=["No changes"],
    )

    response = client.import_organisations(organisations)

    assert adapter.call_count == 1
    assert adapter.last_request.json() == organisations_json
    assert isinstance(response, OrganisationImportResult)
    assert response.insert is None
    assert response.delete is None
    assert response.update is None


def test_import_organisations_missing_parent(
    client: CimClient,
    organisations_missing_parent: Any,
    caplog: Any,
    requests_mock: Any,
) -> None:
    caplog.set_level(level=logging.DEBUG, logger=client_logger.name)
    requests_mock.post(
        client.endpoints.import_organisations(),
        json=["No changes"],
    )

    response = client.import_organisations(organisations_missing_parent)
    assert isinstance(response, OrganisationImportResult)
    assert response.insert is None
    assert response.delete is None
    assert response.update is None

    warning = caplog.records[0]
    assert warning.levelno == logging.WARNING
    assert warning.message == "1 keys not found"

    debug = caplog.records[1]
    assert debug.levelno == logging.DEBUG
    assert debug.message == "Unknown keys: {'does not exist'}"


def test__find_unknown_parents(organisations_missing_parent: Any) -> None:
    unknown = _find_unknown_parents(organisations_missing_parent)
    assert unknown == {organisations_missing_parent[1].parent_key}


def test__serialise_upsert_person() -> None:
    p = Person.construct(
        firstname="Ola",
        lastname="Dunk",
        username="donkey",
    )
    s = _serialise_upsert_person(p)
    keys = s.keys()
    assert "job_mobile" not in keys
    assert "job_phone" not in keys
    assert "secret_number" not in keys
    assert "private_mobile" not in keys
    assert "private_phone" not in keys
    assert "person_type" not in keys


def test__serialise_upsert_person_wrong_type_raises_type_error() -> None:
    p = "Hey babiluba"
    with pytest.raises(TypeError):
        _serialise_upsert_person(p)  # type: ignore

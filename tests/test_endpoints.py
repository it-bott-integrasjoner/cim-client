from cim_client.client import CimEndpoints


def test_init(baseurl: str) -> None:
    endpoints = CimEndpoints(baseurl)
    assert endpoints.baseurl == baseurl


def test_upsert_persons(baseurl: str, endpoints: CimEndpoints) -> None:
    url = endpoints.upsert_persons()
    assert url == baseurl + "?ws=contacts/upsert/1.0"


def test_delete_persons(baseurl: str, endpoints: CimEndpoints) -> None:
    url = endpoints.delete_persons()
    assert url == baseurl + "?ws=contacts/delete/1.0"


def test_get_upsert_persons_schema(baseurl: str, endpoints: CimEndpoints) -> None:
    url = endpoints.get_upsert_persons_schema()
    assert url == baseurl + "?ws=contacts/upsert/1.0"


def test_get_delete_persons_schema(baseurl: str, endpoints: CimEndpoints) -> None:
    url = endpoints.get_delete_persons_schema()
    assert url == baseurl + "?ws=contacts/delete/1.0"


def test_custom_upsert_persons(custom_endpoints: CimEndpoints, baseurl: str) -> None:
    assert (
        custom_endpoints.upsert_persons() == baseurl + "custom/?ws=contacts/upsert/1.0"
    )


def test_custom_delete_persons(custom_endpoints: CimEndpoints, baseurl: str) -> None:
    assert (
        custom_endpoints.delete_persons() == baseurl + "custom/?ws=contacts/delete/1.0"
    )


def test_custom_get_upsert_persons_schema(
    custom_endpoints: CimEndpoints, baseurl: str
) -> None:
    assert (
        custom_endpoints.get_upsert_persons_schema()
        == baseurl + "custom/?ws=contacts/upsert/1.0"
    )


def test_custom_get_delete_persons_schema(
    custom_endpoints: CimEndpoints, baseurl: str
) -> None:
    assert (
        custom_endpoints.get_delete_persons_schema()
        == baseurl + "custom/?ws=contacts/delete/1.0"
    )

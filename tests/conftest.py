import json
import os
from typing import Any, List

import pytest

from cim_client.client import CimClient, CimEndpoints
from cim_client.models import Organisation


def load_json_file(name: str) -> Any:
    """Load json file from the fixtures directory"""
    here = os.path.realpath(
        os.path.join(os.getcwd(), os.path.dirname(__file__).rsplit("/", 1)[0])
    )
    with open(os.path.join(here, "tests/fixtures", name)) as f:
        data = json.load(f)
    return data


@pytest.fixture
def baseurl() -> str:
    return "https://localhost/_webservices/"


@pytest.fixture
def endpoints(baseurl: str) -> CimEndpoints:
    return CimEndpoints(baseurl)


@pytest.fixture
def custom_endpoints(baseurl: str) -> CimEndpoints:
    return CimEndpoints(
        baseurl,
        "custom/?ws=contacts/upsert/1.0",
        "custom/?ws=contacts/delete/1.0",
    )


@pytest.fixture
def client(baseurl: str) -> CimClient:
    return CimClient(CimEndpoints(baseurl))


@pytest.fixture
def john_doe() -> Any:
    return load_json_file("johndoe.json")


@pytest.fixture
def jane_doe() -> Any:
    return load_json_file("janedoe.json")


@pytest.fixture
def fai_lure() -> Any:
    return load_json_file("failure.json")


def load_organisations(file: str) -> List[Organisation]:
    return list(map(Organisation.from_dict, load_json_file(file)))


@pytest.fixture
def organisations() -> List[Organisation]:
    return load_organisations("organisations.json")


@pytest.fixture
def organisations_json() -> Any:
    return load_json_file("organisations.json")


@pytest.fixture
def organisations_missing_parent() -> Any:
    return load_organisations("organisations_missing_parent.json")


@pytest.fixture
def config_example() -> Any:
    return load_json_file("../../example-config.json")["client"]

"""Models used by the client"""

import json
from enum import Enum
from typing import TypeVar, Optional, List, Type, Dict, Any

import pydantic.networks
from pydantic import Extra

T = TypeVar("T")
BaseModel_T = TypeVar("BaseModel_T", bound="BaseModel")
PersonBase_T = TypeVar("PersonBase_T", bound="PersonBase")

NameType = TypeVar("NameType")


class BaseModel(pydantic.BaseModel):
    """Expanded BaseModel for convenience"""

    @classmethod
    def from_dict(cls: Type[BaseModel_T], data: Dict[str, Any]) -> BaseModel_T:
        """Initialize class from dict"""
        return cls(**data)

    @classmethod
    def from_json(cls: Type[BaseModel_T], json_data: str) -> BaseModel_T:
        """Initialize class from json file"""
        data = json.loads(json_data)
        return cls.from_dict(data)

    class Config:
        extra = Extra.forbid
        validate_assignment = True


class PersonTypeEnum(str, Enum):
    user = "user"
    contact = "contact"


class RelationEnum(str, Enum):
    marriage_partner = "marriage_partner"
    cohabitant = "cohabitant"
    child = "child"
    sibling = "sibling"
    parent = "parent"
    grandparent = "grandparent"
    grandchild = "grandchild"
    other_relations = "other_relations"
    unknown = "unknown"


class NextOfKin(BaseModel):
    firstname: str
    lastname: str
    id: str
    phone: Optional[str]
    email: Optional[str]
    relation: RelationEnum = RelationEnum.unknown
    # Date format must agree with system admin
    date_of_birth: str = ""
    address: str = ""
    state: str = ""
    postal_code: str = ""
    town: str = ""
    # ISO 3166 two-letter country code (alpha-2)
    country_code: str = ""

    @pydantic.validator(
        "date_of_birth",
        "address",
        "state",
        "postal_code",
        "town",
        "country_code",
        pre=True,
    )
    def validate_parent_key(cls, value: Optional[str]) -> str:  # noqa:[N805]
        return "" if value is None else value


class NextOfKinMixin(BaseModel):
    next_of_kin: List[NextOfKin] = []


class OrgImportIdMixin(BaseModel):
    org_import_id: str = ""


class PersonBase(BaseModel):
    """Main model containing all fields for a CIM Person"""

    # Unique identifier if not user_import_id is activated
    username: str
    lastname: str
    firstname: str
    # Set person as user or contact. The settings on this installation
    # will decide the type if not set.
    person_type: Optional[PersonTypeEnum]
    job_mobile: str = ""
    job_phone: str = ""
    secret_number: str = ""
    private_mobile: str = ""
    private_phone: str = ""
    email: Optional[str]
    email_secondary: Optional[str]
    job_title: str = ""
    employee_id: str = ""
    description: str = ""
    # Comma separated list ('foo' or 'foo,bar,baz')
    roles: str = ""
    # List of dist_lists, same as above
    dist_list: Optional[str]
    # This is actually an enum of allowed values, see schema
    timezone: Optional[str]

    @pydantic.validator(
        "job_mobile",
        "job_phone",
        "secret_number",
        "private_mobile",
        "private_phone",
        "job_title",
        "employee_id",
        "description",
        "roles",
        pre=True,
    )
    def validate_parent_key(cls, value: Optional[str]) -> str:  # noqa:[N805]
        return "" if value is None else value


class PersonList(BaseModel):
    """Model for making json formatted list of persons"""

    persons: List[PersonBase]


class Organisation(BaseModel):
    """Cim organisation"""

    name: str
    key: str
    parent_key: Optional[str]

    @pydantic.validator("parent_key", pre=True)
    def validate_parent_key(cls, value: Optional[str]) -> Optional[str]:  # noqa:[N805]
        return None if value == "" else value


class OrganisationList(BaseModel):
    __root__: List[Organisation]


class OrganisationImportResult(BaseModel):
    """API result"""

    insert: Optional[List[str]]
    update: Optional[List[str]]
    delete: Optional[List[str]]

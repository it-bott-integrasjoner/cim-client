# cim-client

[![pipeline status](https://git.app.uib.no/it-bott-integrasjoner/cim-client/badges/master/pipeline.svg)](https://git.app.uib.no/it-bott-integrasjoner/cim-client/-/commits/master)
[![coverage report](https://git.app.uib.no/it-bott-integrasjoner/cim-client/badges/master/coverage.svg)](https://git.app.uib.no/it-bott-integrasjoner/cim-client/-/commits/master)

## Set up development environment

### Virtual environment

#### Create

```bash
virtualenv --python=python3 venv
```

#### Activate

```bash
source venv/bin/activate
```

### Install dependencies

```bash
pip install -r requirements.txt -r requirements-dev.txt -r requirements-test.txt
```

This should install all dependencies, including development dependencies (code formatter, linters etc).

## Running the tests

### Run all tests

Use [tox](https://tox.readthedocs.io/) to run all tests and linters.

```bash
tox
```

### Run pytest

```bash
python -m pytest
```

### Type check

We use static type checker [mypy](http://mypy-lang.org/).

```bash
python -m mypy -p cim_client
```

## Example

Python client for accessing the CIM-API. 

```python
from cim_client import CimClient, CimEndpoints
from cim_client.models import PersonBase, PersonList
c = CimClient(
    CimEndpoints(
        url='https://example.com',
        upsert_persons_url='/_webservices/?ws=contacts/upsert/1.0',
        delete_persons_url='/_webservices/?ws=contacts/delete/1.0',
    ),
)

upd_schema = c.get_upsert_persons_schema()
del_schema = c.get_delete_persons_schema()

person = PersonBase.from_dict({'username': 'foo', 'firstname': 'John', 'lastname': 'Doe'})
response1 = c.upsert_persons(person)

person2 = PersonBase.from_dict({'username': 'bar', 'firstname': 'Petter', 'lastname': 'Smart'})
persons = [person, person2] 
personlist = PersonList(persons=persons)
# Note that delete_person supports both PersonList and [Person, Person, ...]
response2 = c.upsert_persons(personlist)
response3 = c.delete_persons(person.username)
```
